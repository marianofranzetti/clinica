﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    class TipoEmpleado
    {

        private int id { get; set; }
        private string descripcion { get; set; }
        private Boolean estado { get; set; }

        public TipoEmpleado()
        {
        }

        public TipoEmpleado(int id, string descripcion, bool estado)
        {
            this.id = id;
            this.descripcion = descripcion;
            this.estado = estado;
        }
    }
}
