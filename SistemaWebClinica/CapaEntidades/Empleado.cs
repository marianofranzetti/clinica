﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    class Empleado
    {
        private int id { get; set; }
        private TipoEmpleado tipoempleado { get; set; }
        private string nombre { get; set; }
        private string apellidoPaterno { get; set; }
        private string apellidoMaterno { get; set; }
        private string numeroDocumento { get; set; }
        private Boolean estado { get; set; }
        private string imagen { get; set; }
        private string usuario { get; set; }
        private string clave { get; set; }


        public Empleado() { }

        public Empleado(int id, TipoEmpleado tipoempleado, string nombre, string apellidoPaterno, string apellidoMaterno, string numeroDocumento, bool estado, string imagen, string usuario, string clave)
        {
            this.id = id;
            this.tipoempleado = tipoempleado;
            this.nombre = nombre;
            this.apellidoPaterno = apellidoPaterno;
            this.apellidoMaterno = apellidoMaterno;
            this.numeroDocumento = numeroDocumento;
            this.estado = estado;
            this.imagen = imagen;
            this.usuario = usuario;
            this.clave = clave;
        }
    }
}
