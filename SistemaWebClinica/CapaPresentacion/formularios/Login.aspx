﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="formularios_Login" %>

<!DOCTYPE html>

<html class="bg-black" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Clinica San Martin</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
    <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />
</head>
<body class="bg-black">
    <div class="alert alert-primary" role="alert" style="text-align: center; display:block;margin:auto; font-family: 'Californian FB'; font-size: 40px">
        <img src="../img/logito.png" style="width: 50px; height: 50px" />
        Diamonds Unity
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="form-box" id="login-box" style="text-align: center;">
        <div class="header bg-light-blue" style="text-align: center;">Login</div>
        <form id="form1" runat="server">
            <div class="body bg-aqua">
                <div class="form-group" style="text-align: center">
                    <asp:TextBox ID="textboxusuario" runat="server" CssClass="form-control" placeholder="Ingrese Usuario"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="textboxpassword" runat="server" CssClass="form-control" placeholder="Ingrese Password"></asp:TextBox>
                </div>

            </div>
            <div class="footer">
                <asp:Button ID="btnIngresar" runat="server" Text="Iniciar Sesion" CssClass="btn bg-light-blue btn-block" OnClick="btnIngresar_Click" />
            </div>
        </form>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
